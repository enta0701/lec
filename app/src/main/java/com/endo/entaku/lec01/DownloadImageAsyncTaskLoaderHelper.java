package com.endo.entaku.lec01;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by entaku on 15/05/02.
 */
public class DownloadImageAsyncTaskLoaderHelper extends AsyncTaskLoader<Bitmap> {
    private String url = "";
    private Context context = null;


    public DownloadImageAsyncTaskLoaderHelper(Context context, String url) {
        super(context);

        this.url = url;
        this.context = context;
    }

    @Override
    public Bitmap loadInBackground() {
        try {
            return HttpUtil.getBitmapHttpService(context, url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
