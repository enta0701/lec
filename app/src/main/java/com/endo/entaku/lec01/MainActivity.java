package com.endo.entaku.lec01;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;


public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks<Bitmap> {

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_main);

        // AsyncTask をスタートさせる
        startAsyncLoadImage("http://surtrek.jp/southamericandream/wp/wp-content/uploads/2011/12/uyuni.jpg");

    }

    public void startAsyncLoadImage(String url) {
        Bundle args = new Bundle();
        args.putString("url", url);
        getLoaderManager().initLoader(0, args, this);    // onCreateLoaderが呼ばれます

        // 複数のLoaderを同時に動かす場合は、第一引数を一意のIDにしてやる必要があります。
        // GridViewなどに表示する画像を非同期で一気に取得する場合とか
    }

    @Override
    public Loader<Bitmap> onCreateLoader(int id, Bundle args) {

        // 非同期で処理を実行するLoaderを生成します.
        // ここを切り替えてあげるだけで様々な非同期処理に対応できます.
        if(args != null) {
            String url = args.getString("url");
            return new DownloadImageAsyncTaskLoaderHelper(this, url);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Bitmap> arg0, Bitmap arg1) {

        // 非同期処理が終了したら呼ばれます.
        // 今回はDownloadが完了した画像をImageViewに表示します.
        ImageView imageView = (ImageView)findViewById(R.id.img);
        Drawable iconImage = new BitmapDrawable(getResources(), arg1);
        imageView.setImageDrawable(iconImage);
        imageView.invalidate();
    }

    @Override
    public void onLoaderReset(Loader<Bitmap> arg0) {

    }
}
